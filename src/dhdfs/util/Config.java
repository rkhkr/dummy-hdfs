package dhdfs.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import dhdfs.filehandler.FileInfo;

public class Config {

	public static String SERVLET_ABS_PATH = null;
	public static final String SERVLET_SERVER_DIR = "C:\\Servlet";
	public static final String SERVER_STORAGE = "C:\\LBServers\\";
	public static final String SEPARATOR = File.separator;
	public static final String DETAIL_FILE = SERVLET_SERVER_DIR + SEPARATOR + "details.json";

	public static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");

	public static final short SERVER_COUNT = 5;
	public static final int PAUSE_TIME = 0;

	public static final String Ciphertext_Suffix = ".cpabe";
	public static final String PublicKey_Suffix = ".pk";
	public static final String MasterKey_Suffix = ".mk";
	public static final String SecretKey_Suffix = ".sk";

	public static final Map<String, FileInfo> dataMap = new HashMap<String, FileInfo>();

	public static void deleteDir(File dir) {
		if (dir.exists()) {
			if (dir.isDirectory()) {
				File[] children = dir.listFiles();
				if (children.length == 0) {
					dir.delete();
				} else {
					for (int i = 0; i < children.length; i++) {
						deleteDir(children[i]);
					}
				}
			} else {
				dir.delete();
			}
		}
	}

	public static final void createDir(String dirs) {
		try {
			createDir(dirs, false);
		} catch (Exception e) {
			System.out.println("Exception::Config:" + e.getMessage());
		}
	}

	public static final void createDir(String dirs, boolean deleteOldDir) throws Exception {
		File file = new File(dirs);
		if (file.exists() && file.isDirectory()) {
			if (deleteOldDir) {
				deleteDir(file);
				file.mkdirs();
			}
		} else {
			file.mkdirs();
		}
	}

	public static String removeExt(String fileName) {
		if (fileName.indexOf(".") > 0)
			fileName = fileName.substring(0, fileName.lastIndexOf("."));
		return fileName;
	}

	public static String getDateForFileName(Date date) {
		return new SimpleDateFormat("ddMMyyyyHHmmss").format(date);
	}

	public final static void threadSleep() {
		try {
			Thread.sleep(PAUSE_TIME * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}