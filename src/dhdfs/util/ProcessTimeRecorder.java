package dhdfs.util;

public class ProcessTimeRecorder {

	private String timerName = null;
	private long startTimeInMiliSec = -1;
	private long endTimeInMiliSec = -1;

	public ProcessTimeRecorder(String timerName) {
		this.timerName = timerName;
	}

	public void recordStartTime() throws Exception {
		if (startTimeInMiliSec == -1)
			startTimeInMiliSec = System.currentTimeMillis();
		else
			throw new Exception("Start time already set !");
	}

	public long recordEndTime() throws Exception {
		endTimeInMiliSec = System.currentTimeMillis();
		return getTimeDiffInMiliSec();
	}

	public long getTimeDiffInMiliSec() throws Exception {
		if (startTimeInMiliSec == -1 || endTimeInMiliSec == -1)
			throw new Exception("StartTime or endtime not set yet !");
		return ((endTimeInMiliSec - startTimeInMiliSec) / 100) * 20;
	}
}
