package dhdfs.filehandler;

import com.alibaba.fastjson.JSONObject;

public class FileInfo {
	long fileSize = 0;
	short numberOfParts = 0;
	String dataDir = null;
	String fileName = null;
	long upt = 0, dpt = 0;
	String uploadedTime = null;

	public FileInfo() {

	}

	public FileInfo(JSONObject jobj) {
		for (String key : jobj.keySet()) {
			String value = jobj.getString(key);
			switch (key) {
			case "filename":
				setFileName(value);
				break;
			case "filesize":
				setFileSize(Long.parseLong(value));
				break;
			case "upt":
				setUPT(Long.parseLong(value));
				break;
			case "dpt":
				setDPT(Long.parseLong(value));
				break;
			case "datadir":
				setDataDir(value);
				break;
			case "fileparts":
				setNumberOfParts(Short.valueOf(value));
				break;
			case "uploadtime":
				setUploadTime(value);
				break;
			default:
				break;
			}
		}
	}

	public FileInfo(String orignalFileName) {
		setFileName(orignalFileName);
	}

	public void setFileName(String orignalFileName) {
		this.fileName = orignalFileName;
	}

	public void setUploadTime(String time) {
		uploadedTime = time;
	}

	public void setDataDir(String dir) {
		dataDir = dir;
	}

	public void setNumberOfParts(short parts) {
		numberOfParts = parts;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}
	
	public void setUPT(long upt) {
		this.upt = upt;
	}
	
	public void setDPT(long dpt) {
		this.dpt = dpt;
	}
	
	public String getFileName() {
		return this.fileName;
	}

	public String getUploadTime() {
		return this.uploadedTime;
	}
	
	public String getDataDir() {
		return this.dataDir;
	}
	
	public short getFilePartsCount() {
		return this.numberOfParts;
	}
	
	public long getFileSize() {
		return this.fileSize;
	}
	
	public long getUPT() {
		return this.upt;
	}
	
	public long getDPT() {
		return this.dpt;
	}
	
	public JSONObject getJsonObject() {
		JSONObject jobj = new JSONObject();
		jobj.put("filename", fileName);
		jobj.put("filesize", fileSize);
		jobj.put("upt", upt);
		jobj.put("dpt", dpt);
		jobj.put("datadir", dataDir);
		jobj.put("fileparts", numberOfParts);
		jobj.put("uploadtime", uploadedTime);
		return jobj;
	}
	
	@Override
	public String toString() {
		return getJsonObject().toJSONString();
	}

}
