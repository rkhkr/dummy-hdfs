package dhdfs.filehandler;

import java.io.*;
import java.util.Arrays;

import dhdfs.servers.LBServer;
import dhdfs.servers.LBServerHandler;
import dhdfs.util.Config;

public class FileSplitter {

	int serverCount = 0;
	String encryptFileName = null;
	String dataDirName = null;
	LBServerHandler serverHandler = null;

	public FileSplitter(String encryptFileName, String dataDirName, int serverCount) throws Exception {
		if (serverCount == 0)
			throw new Exception("must be provide atleast 1 server");
		this.serverCount = serverCount;
		this.encryptFileName = encryptFileName;
		this.dataDirName = dataDirName;
		serverHandler = LBServerHandler.getInstance();
	}

	public void split() throws Exception {
		File file = new File(encryptFileName);
		if (!file.exists())
			throw new Exception("Encryption file: " + encryptFileName + " not found!");

		byte[] bytesArray = new byte[(int) file.length()];
		FileInputStream fis = new FileInputStream(file);
		fis.read(bytesArray); // read file into bytes[]
		fis.close();

		long fileSizeInBytes = bytesArray.length;
		
		System.out.println("Encrypted file size in bytes: " + fileSizeInBytes + " bytes");
		System.out.println("Load balancing server count: " + serverCount);
		
		int eachPartSize = (int) Math.ceil(fileSizeInBytes / (float) serverCount);

		System.out.println("Expected size of each chunks: " + eachPartSize + " bytes");

		String newFileName;
		FileOutputStream filePart = null;
		int nChunks = -1, startIndex = 0, endIndex = 0;

		try {
			for(int index = 0; index < serverCount; index++) {
				String serverName = serverHandler.serverList.get(index);
				LBServer server = serverHandler.serverMap.get(serverName);
				
				System.out.println("\nServer name: " + serverName);
				
				String fileDir = server.getServerStoreagePath() + Config.SEPARATOR + dataDirName;
				Config.createDir(fileDir);
				
				System.out.println("Chunk directory: " + fileDir);
				
				if (fileSizeInBytes < eachPartSize)
					eachPartSize = (int) fileSizeInBytes;

				System.out.println("Creating file part-" + index + " ......");
				Config.threadSleep();
				
				endIndex += eachPartSize;
				byte[] temp = Arrays.copyOfRange(bytesArray, startIndex, endIndex);
				startIndex += temp.length;
				fileSizeInBytes -= temp.length;
				newFileName = fileDir + Config.SEPARATOR + file.getName() + ".part" + (++nChunks);
				filePart = new FileOutputStream(new File(newFileName));
				filePart.write(temp);

				System.out.println("File Part-" + index + " created!");
				System.out.println("Part-" + nChunks + " file name: " + newFileName);
				System.out.println("Part-" + nChunks + " file size: " + temp.length + " bytes\n");

				filePart.flush();
				filePart.close();
				filePart = null;
			}
			if (!file.delete())
				throw new Exception("File can't delete !");
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}
}
