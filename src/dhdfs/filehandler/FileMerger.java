package dhdfs.filehandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

import dhdfs.util.Config;

public class FileMerger {

	List<String> encryptFilesPathList = null;
	String encryptOutFileNameWithPath = null;

	public FileMerger(List<String> encryptFilesPathList, String encryptOutFileNameWithPath) {
		this.encryptFilesPathList = encryptFilesPathList;
		this.encryptOutFileNameWithPath = encryptOutFileNameWithPath;
	}

	public String merge() {
		System.out.println("Creating empty encrypted single file on temp directory .... ");
		Config.threadSleep();
		
		File outFile = new File(encryptOutFileNameWithPath);
		FileOutputStream fos = null;
		FileInputStream fis = null;
		byte[] fileBytes;
		int bytesRead = 0;
		int fileTempIndex = 0;
		try {
			fos = new FileOutputStream(outFile, true);
			for (String filePartName : encryptFilesPathList) {
				File file = new File(filePartName);
				fis = new FileInputStream(file);
				fileBytes = new byte[(int) file.length()];
				bytesRead = fis.read(fileBytes, 0, (int) file.length());
				assert (bytesRead == fileBytes.length);
				assert (bytesRead == (int) file.length());
				
				System.out.println("Reading file part-" + ++fileTempIndex + " ....");
				Config.threadSleep();
				
				fos.write(fileBytes);
				fos.flush();
				fileBytes = null;
				fis.close();
				fis = null;
			}
			fos.close();
			fos = null;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return encryptOutFileNameWithPath;
	}
}
