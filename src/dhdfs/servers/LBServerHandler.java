package dhdfs.servers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dhdfs.util.Config;

public class LBServerHandler { // Singleton class

	private static LBServerHandler instance = null;
	public List<String> serverList = new ArrayList<String>();
	public Map<String, LBServer> serverMap = null;

	private LBServerHandler() throws Exception {
		
		int serverCount = Config.SERVER_COUNT;
		if(serverCount < 2)
			throw new Exception("Must be provide atleast 2 server");
		
		serverMap = new HashMap<String, LBServer>();
		
		for(int index = 0; index < serverCount; index++){
			String serverName = "LBServer00" + index;
			LBServer lbServer = new LBServer(serverName);
			serverList.add(index, serverName);
			serverMap.put(serverName, lbServer);
		}
		
		System.out.println("started \"Load-Balancing\" servers !");
	}

	public static LBServerHandler getInstance() {
		if (instance == null)
			try {
				instance = new LBServerHandler();
			} catch (Exception e) {
				System.out.println("Execption::LBServerHandler:" + e.getMessage());
			}
		return instance;
	}

}
