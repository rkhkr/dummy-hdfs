package dhdfs.servers;

import dhdfs.util.Config;

public class LBServer {
	String serverName;
	String storagePath = Config.SERVER_STORAGE;

	public LBServer(String serverName) throws Exception {
		this.serverName = serverName;
		storagePath += serverName;
		Config.createDir(storagePath);
	}

	public String getServerName() {
		return serverName;
	}

	public String getServerStoreagePath() {
		return storagePath;
	}
}
