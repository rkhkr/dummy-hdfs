package dhdfs.servlet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import dhdfs.cpabe.Crypto;
import dhdfs.filehandler.FileSplitter;
import dhdfs.filehandler.FileInfo;
import dhdfs.servers.LBServerHandler;
import dhdfs.util.Config;
import dhdfs.util.ProcessTimeRecorder;

public class FileUploadServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	LBServerHandler serverHandler = null;

	@Override
	public void init() throws ServletException {
		super.init();
		
		System.out.println("Servlet server started !");
		
		try {

			System.out.println("Checking all files & directories ...");
			if (!new File(Config.DETAIL_FILE).exists() || !new File(Config.SERVLET_SERVER_DIR).exists() || !new File(Config.SERVER_STORAGE).exists()) {

				System.out.println("Creating fresh files & directory ...");
				
				Config.createDir(Config.SERVLET_SERVER_DIR, true);
				Config.createDir(Config.SERVER_STORAGE, true);

				File file = new File(Config.DETAIL_FILE);
				file.createNewFile();
				BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));
				writer.write("[]");
				writer.close();
			}

			serverHandler = LBServerHandler.getInstance();
			Config.SERVLET_ABS_PATH = getServletContext().getRealPath("\\WEB-INF");

			System.out.println("Reading existing details .... ");

			String jsonString = new String(Files.readAllBytes(Paths.get(Config.DETAIL_FILE)));
			jsonString = jsonString.replace("\\", "");
			JSONArray jsonArray = (JSONArray) JSONArray.parse(jsonString);
			for (int index = 0; index < jsonArray.size(); index++) {
				JSONObject jobj = jsonArray.getJSONObject(index);
				FileInfo fileInfo = new FileInfo(jobj);
				Config.dataMap.put(fileInfo.getDataDir(), fileInfo);
			}
			
			System.out.println("details file loaded !");
			System.out.println("Server waiting for the 1st uploading request .........");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ProcessTimeRecorder p1 = new ProcessTimeRecorder("uploadProcessTime");
		System.out.println("\n\n*************** FILE UPLOAD ***************");
		System.out.println("\nGot uploading request from user !");
		
		Date currTime = Calendar.getInstance().getTime();

		if (ServletFileUpload.isMultipartContent(request)) {
			try {
				FileInfo fileInfo = new FileInfo();
				List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);

				for (FileItem item : multiparts) {
					if (!item.isFormField()) {
						
						System.out.println("Fetching uploaded file info .....");
						
						String fileName = item.getName();
						fileInfo.setFileName(fileName);
						
						System.out.println("Uploaded file name: " + fileName);
						
						long fileSize = item.getSize();
						fileInfo.setFileSize(fileSize);
						System.out.println("Uploaded file size: " + fileSize + " KB");

						String formattedTime = Config.DATE_FORMATTER.format(currTime);
						fileInfo.setUploadTime(formattedTime);

						System.out.println("Uploaded time: " + formattedTime);

						String dataDirName = Config.removeExt(fileName) + "@" + Config.getDateForFileName(currTime);

						System.out.println("Generated file unique ID: " + dataDirName);
						System.out.println("Created unique directory using unique ID ....");
						
						Config.threadSleep();
						
						String dirNameWithPath = Config.SERVLET_SERVER_DIR + Config.SEPARATOR + dataDirName;
						Config.createDir(dirNameWithPath, true);
						fileInfo.setDataDir(dataDirName);
						
						System.out.println("Uploading original file .....");
						
						Config.threadSleep();

						String fileNameWithPath = Config.SERVLET_SERVER_DIR + Config.SEPARATOR + dataDirName + Config.SEPARATOR + fileName;
						item.write(new File(fileNameWithPath));

						System.out.println("Original file successfully uploaded on servlet server !");

						p1.recordStartTime();

						System.out.println("Encrypting original uploaded file ......");
						
						Config.threadSleep();

						String encryptFileNameWithPath = Crypto.encrypt(fileNameWithPath); // Data Encryption

						System.out.println("Encryption done on original file !");
						System.out.println("Encrypted file name with server path: "+ encryptFileNameWithPath);
						System.out.println("Splitting encrypted file ......");
						
						FileSplitter fileSplitter = new FileSplitter(encryptFileNameWithPath, dataDirName, Config.SERVER_COUNT); // file splitting
						fileInfo.setNumberOfParts(Config.SERVER_COUNT);
						fileSplitter.split();

						System.out.println("Splitting done on encrypted file !");

						fileInfo.setUPT(p1.recordEndTime());
						fileInfo.setDPT(0);
						Config.dataMap.put(dataDirName, fileInfo);

						System.out.println("Updating details file ....");

						File file = new File(Config.DETAIL_FILE);
						if (!file.exists())
							throw new Exception("Json file not found at loading time !!");
						else {
							String jsonString = new String(Files.readAllBytes(Paths.get(Config.DETAIL_FILE)));
							JSONArray jsonArray = (JSONArray) JSONArray.parse(jsonString);
							jsonArray.add(fileInfo.getJsonObject());
							if (file.delete() && file.createNewFile()) {
								BufferedWriter writer = new BufferedWriter(new FileWriter(Config.DETAIL_FILE, true));
								writer.write(jsonArray.toString());
								writer.close();

								System.out.println("Details file updated !");

							} else {
								throw new Exception("some error occured !");
							}
						}
					}
				}

				System.out.println("File Uploaded Successfully!");
			} catch (Exception ex) {
				System.out.println("Exception on file uploading!!");
			}

		} else {
			System.out.println("Sorry this application only handles file upload request!");
		}
		response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));

	}
}
