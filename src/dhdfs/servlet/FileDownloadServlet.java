package dhdfs.servlet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import dhdfs.cpabe.Crypto;
import dhdfs.filehandler.FileInfo;
import dhdfs.filehandler.FileMerger;
import dhdfs.servers.LBServer;
import dhdfs.servers.LBServerHandler;
import dhdfs.util.Config;
import dhdfs.util.ProcessTimeRecorder;

public class FileDownloadServlet extends HttpServlet {

	LBServerHandler serverHandler = null;
	private static int BUFFER_SIZE = 1024 * 100;

	@Override
	public void init() throws ServletException {
		super.init();
		serverHandler = LBServerHandler.getInstance();
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		System.out.println("\n\n*************** FILE DOWNLOAD ***************");
		System.out.println("\nGot download request from user !");
		
		String dirName = request.getParameter("clickval");
		if (dirName == null || dirName.equals("notDefine"))
			System.out.println("FileDownloadHandler:Dir name is null or dir name can't fetch!");
		else {
			System.out.println("File unique ID: " + dirName);
			System.out.println("Fetching requested file info using unique ID.....");
			
			String tempDir = Config.SERVLET_SERVER_DIR + Config.SEPARATOR + dirName + Config.SEPARATOR + "temp";
			Config.createDir(tempDir);
			
			System.out.println("Created temp directory loc: " + tempDir);

			ProcessTimeRecorder p2 = new ProcessTimeRecorder("downloadProcessTime");
			try {
				p2.recordStartTime();
			} catch (Exception e1) {
				e1.printStackTrace();
			}

			FileInfo fileInfo = Config.dataMap.get(dirName);
			String fileName = fileInfo.getFileName();
			short uploadedServerCount = fileInfo.getFilePartsCount();
			
			System.out.println("Requested file name: " + fileName);
			System.out.println("File parts counts: " + uploadedServerCount);

			List<String> encryptFileNamesList = new ArrayList<String>();

			System.out.println("Start copying all the chunks from LBServers to servlet server ......");
			
			for (short index = 0; index < uploadedServerCount; index++) {
				String serverName = serverHandler.serverList.get(index);
				LBServer lbServer = serverHandler.serverMap.get(serverName);

				System.out.println("\nServer name: " + serverName);
				
				String filePartPath = lbServer.getServerStoreagePath() + Config.SEPARATOR + dirName;
				
				String filePartName = fileName + Config.Ciphertext_Suffix + ".part" + index;
				String filePartNameWithPath = filePartPath + Config.SEPARATOR + filePartName;

				System.out.println("Part-" + index + " file path: " + filePartNameWithPath);
				System.out.println("Copying part-" + index + " to temp directory");

				Config.threadSleep();

				Files.copy(Paths.get(filePartNameWithPath), Paths.get(tempDir + Config.SEPARATOR + filePartName), StandardCopyOption.REPLACE_EXISTING);

				encryptFileNamesList.add(filePartNameWithPath);
			}
			System.out.println("Copied all chunks to temp !");
			System.out.println("\nStart chunks merging on temp directory....");
			
			FileMerger fileMerger = new FileMerger(encryptFileNamesList, tempDir + Config.SEPARATOR + fileName + Config.Ciphertext_Suffix);
			String encryptFileNameWithPath = fileMerger.merge();

			System.out.println("Merging done !");
			System.out.println("Merged output file name: " + encryptFileNameWithPath);
			System.out.println("\nStart decryption .....");
			
			String servletDirPath = Config.SERVLET_SERVER_DIR + Config.SEPARATOR + dirName;
			String PKFileName = servletDirPath + Config.SEPARATOR + Config.removeExt(fileName) + Config.PublicKey_Suffix;
			String SKFileName = servletDirPath + Config.SEPARATOR + Config.removeExt(fileName) + Config.SecretKey_Suffix;
			String outputFile = null;
			try {
				outputFile = Crypto.decrypt(encryptFileNameWithPath, PKFileName, SKFileName);
			} catch (Exception e) {
				e.printStackTrace();
			}

			System.out.println("Decrypted file name with path: " + outputFile);
			
			try {
				fileInfo.setDPT(p2.recordEndTime());

				Cookie ck = new Cookie("file", dirName);//creating cookie object  
				response.addCookie(ck);//adding cookie in the response  
				
				File file = new File(Config.DETAIL_FILE);
				if (!file.exists())
					throw new Exception("Json file not found at updating time !!");
				else {
					String jsonString = new String(Files.readAllBytes(Paths.get(Config.DETAIL_FILE)));
					JSONArray jsonArray = (JSONArray) JSONArray.parse(jsonString);

					int index = 0;
					while(index < jsonArray.size()){
						JSONObject jObj = jsonArray.getJSONObject(index);
						String id = jObj.getString("datadir");
						if(dirName.equals(id))
							break;
						index++;
					}
					jsonArray.remove(index);
					
					jsonArray.add(fileInfo.getJsonObject());
					if (file.delete() && file.createNewFile()) {
						BufferedWriter writer = new BufferedWriter(new FileWriter(Config.DETAIL_FILE, true));
						writer.write(jsonArray.toString());
						writer.close();

						System.out.println("Details file updated !");

					} else {
						throw new Exception("some error occured !");
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			File oFile = new File(outputFile);
			if (oFile.exists()) {
				OutputStream outStream = null;
		        FileInputStream inputStream = null;
				 
	            //**** Setting The Content Attributes For The Response Object ****/
	            response.setContentType("application/octet-stream");
	 
	            /**** Setting The Headers For The Response Object ****/
	            response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", oFile.getName()));

	            try {
	                /**** Get The Output Stream Of The Response ****/
	                outStream = response.getOutputStream();
	                inputStream = new FileInputStream(oFile);
	                byte[] buffer = new byte[BUFFER_SIZE];
	                int bytesRead = -1;
	 
	                /**** Write Each Byte Of Data Read From The Input Stream Write Each Byte Of Data  Read From The Input Stream Into The Output Stream ****/
	                while ((bytesRead = inputStream.read(buffer)) != -1) {
	                    outStream.write(buffer, 0, bytesRead);
	                }               
	                
	                System.out.println("Decrypted file sending to user .....");

	            } catch(IOException ioExObj) {
	                System.out.println("Exception While Performing The I/O Operation?= " + ioExObj.getMessage());
	            } finally {             
	                if (inputStream != null) {
	                    inputStream.close();
	                }
	 
	                outStream.flush();
	                if (outStream != null) {
	                    outStream.close();
	                }
	                
	                System.out.println("Cleaning temp directory.....");

	                Config.deleteDir(new File(tempDir));// delete temp directory
	            }
	        } else {
	        	Config.deleteDir(new File(tempDir));// delete temp directory

	            /***** Set Response Content Type *****/
	            response.setContentType("text/html");
	 
	            /***** Print The Response *****/
	            response.getWriter().println("<h3>File "+ fileName +" Is Not Present .....!</h3>");
	        }
		}
	}
}
