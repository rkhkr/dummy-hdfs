package dhdfs.cpabe;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.crypto.Cipher;
import dhdfs.util.Config;
import dhdfs.util.ResponseMsg;
import it.unisa.dia.gas.jpbc.Element;

public class Crypto {
	private final static String policy = "2 of (北京大学,软件学院,研究生)";
	private final static String[] attrs = new String[] { "北京大学", "软件学院" };

	private Crypto() {
	}

	public static String encrypt(String fileNameWithPath) throws Exception {

		if (!new File(fileNameWithPath).exists())
			throw new FileNotFoundException();

		String withoutExtFileNameWithPath = Config.removeExt(fileNameWithPath);
		String ciphertextFileName = fileNameWithPath + Config.Ciphertext_Suffix; // Encryp file name
		String PKFileName = withoutExtFileNameWithPath + Config.PublicKey_Suffix; // Public Key file name
		String MKFileName = withoutExtFileNameWithPath + Config.MasterKey_Suffix; // Master Key file name
		String SKFileName = withoutExtFileNameWithPath + Config.SecretKey_Suffix; // Secret Key file name
		setup(PKFileName, MKFileName);

		try {
			enc(fileNameWithPath, policy, ciphertextFileName, PKFileName);
			keygen(attrs, PKFileName, MKFileName, SKFileName);
			return ciphertextFileName;
		} catch (Exception e) {
			return null;
		}
	}

	public static String decrypt(String ciphertextFileName, String PKFileName, String SKFileName) throws Exception {
			return dec(ciphertextFileName, PKFileName, SKFileName);
	}

	private static void setup(String PKFileName, String MKFileName) {
		CPABEImpl.setup(PKFileName, MKFileName);
	}

	private static void enc(String encFileName, String policy, String outputFileName, String PKFileName) throws Exception {
		System.out.println("File encryption processing ....");

		File encFile = new File(encFileName);

		if (!encFile.exists()) {
			throw new Exception(ResponseMsg.Error_EncFile_Missing);
		}

		if (encFile.isDirectory()) {
			throw new Exception(ResponseMsg.Error_Enc_Directory);
		}

		System.out.println("Generate public key ....");
		
		Config.threadSleep();
		
		PublicKey PK = SerializeUtils.unserialize(PublicKey.class, new File(PKFileName));
		if (PK == null) {
			throw new Exception(ResponseMsg.Error_PK_Missing);
		}

		Parser parser = new Parser();
		Policy p = parser.parse(policy);
		if (p == null) {
			throw new Exception(ResponseMsg.Error_Policy_Missing);
		}

		CPABEImpl.enc(encFile, p, PK, outputFileName);

		System.out.println("Deleting original file ....");
		Config.threadSleep();
		// delete original file
		File orignalFile = new File(encFileName);
		if (orignalFile.exists() && orignalFile.isFile()){
			orignalFile.delete();
			System.out.println("Deleted file name: " + encFileName);
		}
	}

	private static void keygen(String[] attrs, String PKFileName, String MKFileName, String SKFileName)	throws Exception {
		System.out.println("Generating  & storing keys ....");
		Config.threadSleep();
		
		if (attrs == null || attrs.length == 0) {
			throw new Exception(ResponseMsg.Error_Attributes_Missing);
		}
		PublicKey PK = SerializeUtils.unserialize(PublicKey.class, new File(PKFileName));
		if (PK == null) {
			throw new Exception(ResponseMsg.Error_PK_Missing);
		}
		MasterKey MK = SerializeUtils.unserialize(MasterKey.class, new File(MKFileName));
		CPABEImpl.keygen(attrs, PK, MK, SKFileName);
		System.out.println("Keys generated & stored successfully !");
	}

	private static String dec(String ciphertextFileName, String PKFileName, String SKFileName) throws Exception {
		DataInputStream dis = null;
		try {
			dis = new DataInputStream(new FileInputStream(new File(ciphertextFileName)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		System.out.println("Load public & Secret keys form servlet server.....");
		
		Ciphertext ciphertext = SerializeUtils._unserialize(Ciphertext.class, dis);
		if (ciphertext == null) {
			throw new Exception(ResponseMsg.Error_Ciphertext_Missing);
		}
		PublicKey PK = SerializeUtils.unserialize(PublicKey.class, new File(PKFileName));
		if (PK == null) {
			throw new Exception(ResponseMsg.Error_PK_Missing);
		}
		SecretKey SK = SerializeUtils.unserialize(SecretKey.class, new File(SKFileName));
		if (SK == null) {
			throw new Exception(ResponseMsg.Error_SK_Missing);
		}

		String decryptFileName = null;
		if (ciphertextFileName.endsWith(".cpabe"))
			decryptFileName = ciphertextFileName.substring(0, ciphertextFileName.length() - 6);
		else
			throw new Exception("Encyp file is not in valid format !");
		
		System.out.println("Expected decrypt file name: " + decryptFileName);
		System.out.println("Decryption Processesing ....");
		System.out.println("Creating decrypted original file on temp directory ... ");
		
		Config.threadSleep();
		
		OutputStream os = new FileOutputStream(CPABEImpl.createNewFile(decryptFileName));
		Element element = CPABEImpl.dec(ciphertext, SK, PK);
		AES.crypto(Cipher.DECRYPT_MODE, dis, os, element);
		os.close();
		dis.close();
		System.out.println("Decryption done !");
		return decryptFileName;
	}
}
