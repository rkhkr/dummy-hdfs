<!DOCTYPE html>
<%@page import="dhdfs.util.Config"%>
<%@page import="dhdfs.filehandler.FileInfo"%>
<%@ page import="java.util.Map"%>
<html>
<head>
<meta charset="ISO-8859-1">
<title>HDFS</title>
<style>
body {
	background-color: lightblue;
}

#downloadInfoTable {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	border-collapse: collapse;
	width: 100%;
}

#downloadInfoTable td, #downloadInfoTable th {
	border: 1px solid #ddd;
	padding: 8px;
}

#downloadInfoTable tr:nth-child(even) {
	background-color: #f2f2f2;
}

#downloadInfoTable tr:hover {
	background-color: #ddd;
}

#downloadInfoTable th {
	padding-top: 12px;
	padding-bottom: 12px;
	text-align: center;
	background-color: #4CAF50;
	color: white;
}

.fsSubmitButton
{
padding: 10px 15px 11px !important;
font-size: 18px !important;
background-color: #57d6c7;
font-weight: bold;
text-shadow: 1px 1px #57D6C7;
color: #ffffff;
border-radius: 5px;
-moz-border-radius: 5px;
-webkit-border-radius: 5px;
border: 1px solid #57D6C7;
cursor: pointer;
box-shadow: 0 1px 0 rgba(255, 255, 255, 0.5) inset;
-moz-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.5) inset;
-webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.5) inset;
}

</style>
<script type="text/javascript">
function switchSubmit(){
	document.getElementById("btn_submit").disabled = false;
}
var downloadTimer;
function downloadReq() {
	let uniqueID = "file=\"" + document.getElementsByName("clickval")[0].value + "\"";
    downloadTimer = window.setInterval( function() {
    	if(document.cookie == uniqueID){
    		window.clearInterval(downloadTimer);
    		document.cookie = "file=;";
	    	document.location.reload(true);
    	}
    }, 1000 );
}
</script>
</head>
<body>
	<div align="center">
		<h1>Hadoop Distributed File System (HDFS)</h1>
	</div>
	

		<div style="width: 50%; float: left">
			<h3>Choose File to Upload in Server</h3>
			<form name="uploadform" action="upload" method="post" enctype="multipart/form-data">
				<input type="file" name="file" oninput="switchSubmit()"/>
				<input id="btn_submit" class="fsSubmitButton" type="submit" value="upload" disabled/>
			</form>
		</div>

		<div style="background: white; float: left; margin-top: 50px;">
			<form name="downloadform" action="download" method="post">
				<input type="hidden" name="clickval" value="notDefined" />
				<table name="downloadInfoTable" id="downloadInfoTable" cellpadding="4" cellspacing="2" border="1">
					<tr>
						<th>FILE-NAME</th>
						<th>FILE SIZE<br/>(KB)</th>
						<th>UP PROCESS TIME<br/>(Milli-Sec)</th>
						<th>UPLOADED TIME</th>
						<th>DOWNLOAD</th>
						<th>DWN PROCESS TIME<br/>(Milli-Sec)</th>
					</tr>
					<%
						for (Map.Entry<String, FileInfo> entry : Config.dataMap.entrySet()) {
							FileInfo fileInfo = entry.getValue();
					%>
					<tr>
						<td><%=fileInfo.getFileName()%></td>
						<td style="text-align: center;"><%=fileInfo.getFileSize()/1000%> KB</td>
						<td style="text-align: center;"><%=fileInfo.getUPT()%> MS</td>
						<td style="text-align: center;"><%=fileInfo.getUploadTime()%></td>
						<td style="text-align: center;"><input type="submit" name="btn_donwload" value="click here"	onclick="{document.downloadform.clickval.value='<%=entry.getKey()%>';downloadReq();}" /></td>
						<td style="text-align: center;"><%=fileInfo.getDPT() == 0 ? "Not downloaded yet!" : fileInfo.getDPT() + " MS"%></td>
					</tr>
					<%
						}
					%>
				</table>
			</form>
		</div>

	
</body>
</html>